﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Newton_s_Method
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            this.btnConfirm.Click += new RoutedEventHandler(btnConfirm_Click);
        }

        class Newton
        {
            private int argument, index, percents;
            private List<double> results;
            private double x0;

            private double F(double x)
            {
                return Math.Pow(x, index) - argument;
            }

            private double Fp(double x)
            {
                return index * Math.Pow(x, index - 1);
            }

            public Newton(int argument, int index, int percents)
            {
                this.argument = argument;
                this.index = index;
                this.percents = percents;
                results = new List<double>();
                x0 = 1.0;
            }

            public double Compute()
            {
                double epsilon = 0.0;
                double xn = 0.0;
                xn = x0 - (F(x0) / Fp(x0));
                results.Add(xn);
                epsilon = Math.Abs(((results[results.Count - 1] - x0) / results[results.Count - 1]))*100;
                while (epsilon > percents)
                {
                    xn = results[results.Count - 1] - (F(results[results.Count - 1])/Fp(results[results.Count - 1]));
                    epsilon = Math.Abs(((xn - results[results.Count - 1]) / results[results.Count - 1]))*100;
                    results.Add(xn);
                }
                return results[results.Count - 1];
            }
        }

        void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (this.TbArgument.Text != "" && this.TbIndex.Text != "" && this.tbPercents.Text != "")
            {
                int argument = Convert.ToInt16(TbArgument.Text);
                int index = Convert.ToInt16(TbIndex.Text);
                int percents = Convert.ToInt16(tbPercents.Text);
                Newton newton = new Newton(argument, index, percents);
                this.TBlockResult.Text = Math.Round(newton.Compute(), 2).ToString();
            }
        }
    }
}